import {Component, OnInit} from '@angular/core';
import {IProduct, ProductService} from '../../services/product.service';
import {products} from '../../shared/PRODUCTS_MOCK';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  // products: IProduct[] = products;
  // productsFiltered: IProduct[] = products;
  products: IProduct[];
  productsFiltered: IProduct[] = [];
  sortDir = 1;
  sortKey = '';

  filterCode = 'code';

  constructor(public productSvc: ProductService,
              private router: Router) {
  }


  ngOnInit() {
    this.productSvc.activeProduct$.next(null);
    this.productSvc.getAll().subscribe((res: IProduct[]) => {
      this.products = res;
      this.productsFiltered = this.products;
    });
  }

  sort(key: string, changeDir = true): void {
    this.sortDir = changeDir && this.sortKey === key ? this.sortDir * -1 : this.sortDir;
    this.sortKey = key;
    this.productsFiltered = this.productsFiltered.sort((a, b) => {
      const aKey = typeof a[key] === 'string' ? a[key].toLowerCase() : a[key];
      const bKey = typeof b[key] === 'string' ? b[key].toLowerCase() : b[key];
      return this.sortDir * (aKey === bKey ? 0 : aKey > bKey ? 1 : -1);
    });
  }

  filterString(e: Event): void {
    const value = (e.target as HTMLInputElement).value;
    this.productsFiltered = this.products.filter(p => {
      return p[this.filterCode].toString().toLowerCase().indexOf(value.toLowerCase()) > -1;
    });
    this.sort(this.sortKey, false);
  }

  showProduct(id: number): void {
    this.router.navigate([id]);
  }
}
