import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {IProduct, IProductControl, ProductService} from '../../services/product.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  formGroup: FormGroup;
  formModel: IProductControl[] = [
    {
      id: 'code',
      label: 'Product code',
      inputType: 'text',
      readOnly: false,
      validators: {
        pattern: /^[1-9]{4}-[1-9]{4}$/
      },
      validationMessage: {
        pattern: 'Code must be as a string like ####-#### where # is a digit in range between 1 to 9.'
      },
    },
    {
      id: 'name',
      label: 'Product name',
      inputType: 'text',
      readOnly: false,
      validators: {
        required: true
      },
      validationMessage: {
        required: 'Name is required.'
      }
    },
    {
      id: 'price',
      label: 'Product price',
      // inputType: 'number',
      inputType: 'text',
      readOnly: false,
      validators: {
        required: true,
        pattern: /^\d+$/
      },
      validationMessage: {
        required: 'Price is required and has to be a number.',
        pattern: 'Price has to be a number.'
      }
    },
  ];


  // products: IProduct[] = products;
  product: IProduct;

  productId: number;
  formSending = false;
  cover: string;

  constructor(public productSvc: ProductService,
              private formBuilder: FormBuilder,
              private toastSvc: ToastrService,
              private router: Router,
              private route: ActivatedRoute) {
    this.errorHandler = this.errorHandler.bind(this);
  }

  ngOnInit() {
    this.productSvc.activeProduct$.next(null);
    this.setFormGroup();
    this.productId = (this.route.snapshot.params as { [key: string]: any }).productId;
    // this.product = this.products.find(p => p.id === +this.productId);

    if (this.productId) {
      this.productSvc.getOne(this.productId).subscribe(product => {
        this.product = product;
        this.productSvc.activeProduct$.next(this.product);

        this.fillForm(this.product);

      });
    }

  }

  setFormGroup() {

    const formControls = this.formModel.reduce((controls, formControl) => {
      const validatorsArr = Object.keys(formControl.validators).reduce((vArr, v) => {

        switch (v) {
          case 'required':
            vArr.push(Validators.required);
            break;
          default:
            vArr.push(Validators.pattern(formControl.validators[v]));
        }

        return vArr;
      }, []);

      controls[formControl.id] = [{value: '', disabled: false}, validatorsArr];

      return controls;

    }, {});

    this.formGroup = this.formBuilder.group(formControls);

  }

  fillForm(product): void {
    this.formModel.forEach(control => {
      if (product[control.id]) {
        this.formGroup.controls[control.id]
          .setValue(product[control.id]);
      }
    });
    this.cover = product.cover ? `http://localhost:3000/products/cover/${product.cover}` : null;
  }

  lockControls(lock: boolean): void {
    this.formModel.forEach((c: IProductControl) => {
      c.readOnly = lock;
    });
  }

  remove(): void {
    if (confirm(`Are you sure you want to remove ${this.product.name}?`)) {
      this.formSending = true;
      this.lockControls(this.formSending);
      this.productSvc.remove(this.productId).subscribe(product => {
        this.toastSvc.success(product.message || `${this.product.name} removed`, '');
        this.router.navigate(['']);
      }, this.errorHandler);
    }

  }

  submit(): void {

    this.formSending = true;
    this.lockControls(this.formSending);
    const body = this.productSvc.getProductBody(this.formGroup.controls as any);

    this.productSvc.save(body, this.productId).subscribe(product => {
      this.formSending = false;
      this.lockControls(this.formSending);
      this.toastSvc.success(product.message || 'Product saved', '');
      this.router.navigate(['']);
    }, this.errorHandler);

  }

  errorHandler(err): void {
    this.formSending = false;
    this.lockControls(this.formSending);
  }

}
