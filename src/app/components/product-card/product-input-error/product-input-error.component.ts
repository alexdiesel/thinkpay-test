import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-product-input-error',
  templateUrl: './product-input-error.component.html',
  styleUrls: ['./product-input-error.component.scss']
})
export class ProductInputErrorComponent {

  @Input() control: AbstractControl;
  @Input() validation: { [key: string]: string };
  @Input() message: { [key: string]: string };

  constructor() {
  }

  get showError(): boolean {
    return (this.control.dirty || this.control.touched)
      && this.control.errors
      && Object.keys(this.control.errors).some(e => this.control.errors[e]);
  }

  get validations(): string[] {
    return Object.keys(this.validation).map(v => v);
  }

}
