import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProductInputErrorComponent } from './product-input-error.component';

describe('ProductInputErrorComponent', () => {
  let component: ProductInputErrorComponent;
  let fixture: ComponentFixture<ProductInputErrorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductInputErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInputErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
