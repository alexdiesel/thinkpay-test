import {Injectable} from '@angular/core';
import {ApiService} from '../shared/services/api.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {map} from 'rxjs/operators';


export interface IProductControl {
  id: string;
  label: string;
  inputType: string;
  readOnly?: boolean;
  validators?: { [key: string]: any };
  validationMessage?: { [key: string]: any };
}

export interface IProduct {
  name: string;
  code: string;
  price: number;

  _id?: number;
  created_at?: string;
  updated_at?: string;
}

export interface IProductResponse {
  data: IProduct | string;
  message: string;
  success: boolean;
}


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  activeProduct$: BehaviorSubject<IProduct> = new BehaviorSubject<IProduct>(null);

  constructor(private apiSvc: ApiService) {
  }

  getAll(): Observable<IProduct[]> {
    return this.apiSvc.get(`products`)
      .pipe(
        map((products: { data: IProduct[] }) => {
          return products.data.sort((a, b) => {
            return a.updated_at === b.updated_at ?
              0 : a.updated_at > b.updated_at ?
                -1 : 1;
          });
        })
      );
  }

  getOne(id: number): Observable<IProduct> {
    return this.apiSvc.get(`products/${id}`)
      .pipe(
        map((product: { data: IProduct }) => product.data)
      );
  }

  save(body: IProduct, id: number = null): Observable<IProductResponse> {
    return id ?
      this.apiSvc.put(`products/${id}`, body) :
      this.apiSvc.post(`products`, body);
  }

  remove(id: number): Observable<IProductResponse> {
    return this.apiSvc.delete(`products/${id}`);
  }

  getProductBody(formGroupControls: { [key: string]: FormControl }): IProduct {
    const body = Object.keys(formGroupControls).reduce((obj, k) => {
      obj[k] = formGroupControls[k].value;
      return obj;
    }, {});
    return body as IProduct;
  }
}
