import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ApiService {

  apiUrl = environment.apiUrl;

  httpOptions: any;

  constructor(private http: HttpClient) {
    this.httpOptions = this.getHttpOptions();
  }

  getHttpOptions(headers: { [key: string]: string } = null): { headers: HttpHeaders; [key: string]: any } {

    const commonHeaders = {
      'Content-Type': 'application/json',
      // Connection: 'close'
    };

    headers = Object.keys(headers || {}).reduce((h, k) => {
      if (headers[k]) {
        h[k] = headers[k];
      }
      return h;
    }, {});

    return headers ?
      {headers: new HttpHeaders({...commonHeaders, ...headers})} :
      {headers: new HttpHeaders(commonHeaders)};
  }

  get(path: string, params?, headers?): Observable<any> {
    const options = this.getHttpOptions(headers);
    if (params) {
      options.params = params;
    }
    return this.http.get(`${this.apiUrl}${path}`, options);
  }

  post(path: string, body: any, headers?): Observable<any> {
    return this.http.post(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body),
      this.getHttpOptions(headers)
    );
  }

  put(path: string, body: any, headers?): Observable<any> {
    return this.http.put(
      `${environment.apiUrl}${path}`,
      JSON.stringify(body),
      this.getHttpOptions(headers)
    );
  }

  delete(path: string, headers?): Observable<any> {
    return this.http.delete(
      `${environment.apiUrl}${path}`,
      this.getHttpOptions(headers)
    );
  }

}
