export const products = [
  {
    id: 66,
    name: 'banana',
    code: '1314-4140',
    price: 15,
    created_at: '2019-10-23 10:18:08',
    updated_at: '2019-11-04 06:27:59'
  },
  {
    id: 67,
    name: 'Apple',
    code: '3333-5550',
    price: 10000,
    created_at: '2019-10-23 11:33:35',
    updated_at: '2019-10-27 13:24:16'
  },
  {
    id: 68,
    name: 'Macbook',
    code: '1234-5678',
    price: 350,
    created_at: '2019-10-23 11:33:58',
    updated_at: '2019-10-27 20:37:22'
  },
  {
    id: 73,
    name: 'Shoes',
    code: '2334-1412',
    price: 90,
    created_at: '2019-10-23 17:34:43',
    updated_at: '2019-11-03 15:58:47'
  },
  {
    id: 76,
    name: 'Candle',
    code: '2345-5999',
    price: 7,
    created_at: '2019-10-23 17:35:29',
    updated_at: '2019-11-03 15:52:02'
  },
  {
    id: 109,
    name: 'strawberry',
    code: '1111-1120',
    price: 10,
    created_at: '2019-10-28 01:32:21',
    updated_at: '2019-10-28 01:40:19'
  },
  {
    id: 121,
    name: 'pineapple',
    code: '1111-1126',
    price: 20,
    created_at: '2019-10-28 09:46:30',
    updated_at: '2019-10-28 09:46:30'
  },
  {
    id: 162,
    name: 'orange',
    code: '1111-2222',
    price: 20,
    created_at: '2019-11-03 14:37:43',
    updated_at: '2019-11-03 14:37:43'
  },
  {
    id: 163,
    name: 'cake',
    code: '2222-2222',
    price: 4,
    created_at: '2019-11-03 14:48:16',
    updated_at: '2019-11-03 14:48:16'
  },
  {
    id: 164,
    name: 'cherry',
    code: '1111-1111',
    price: 10,
    created_at: '2019-11-03 14:50:26',
    updated_at: '2019-11-03 14:50:26'
  },
  {
    id: 165,
    name: 'coffee',
    code: '1234-1234',
    price: 50,
    created_at: '2019-11-03 15:15:16',
    updated_at: '2019-11-03 15:15:16'
  },
  {
    id: 168,
    name: 'tea',
    code: '3434-6767',
    price: 20,
    created_at: '2019-11-03 15:58:04',
    updated_at: '2019-11-03 15:58:04'
  }
];
