import {Component} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import {delay, tap} from 'rxjs/operators';
import {ProductService} from './services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  productName: string;
  update: boolean

  constructor(public productSvc: ProductService,
              swUpdates: SwUpdate) {

    productSvc.activeProduct$.pipe(
      delay(0),
      tap(product => this.productName = product?.name)
    ).subscribe();

    swUpdates.available.subscribe(() => this.update = true);
  }

}
