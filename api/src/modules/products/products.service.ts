import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';

import {Model} from 'mongoose';
import {Product} from './interfaces/products.interface';

@Injectable()
export class ProductsService {

  constructor(@InjectModel('Product') private readonly productModel: Model<Product | any>) {

  }

  async getAll(): Promise<Product[]> {
    return await this.productModel.find();
  }

  async getById(id: string): Promise<Product> {
    return await this.productModel.findOne({_id: id});
  }

  async addProduct(product: Product): Promise<Product> {
    product.cover = Math.floor(Math.random() * 20) + 1 + '.jpeg';
    const createdItem = new this.productModel(product);
    return await createdItem.save();
  }

  async updateProduct(productId: string, updateItem: Product): Promise<Product> {
    updateItem.cover = updateItem.cover || Math.floor(Math.random() * 20) + 1 + '.jpeg';
    updateItem.updated_at = (new Date()).toISOString();
    return await this.productModel.findOneAndUpdate({_id: productId}, updateItem);
  }

  async removeProduct(productId: string): Promise<Product> {
    return await this.productModel.findOneAndDelete({_id: productId});
  }


}
