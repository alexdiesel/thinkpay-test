import {Body, Controller, Delete, Get, Param, Post, Put, Res} from '@nestjs/common';
import * as fs from 'fs';
import {CreateProductsDto} from './dto/create-products.dto';
import {Product} from './interfaces/products.interface';
import {ProductsService} from './products.service';


@Controller('products')
export class ProductsController {

  constructor(private itemsSvc: ProductsService) {
  }

  @Get()
  async getItems(): Promise<Product[]> {
    const data = await this.itemsSvc.getAll();
    return {
      success: true,
      data: data || [],
      message: 'Products retrieved successfully',
    } as any;
  }

  @Get(':id')
  async getItem(@Param('id') id): Promise<any> {
    const data = await this.itemsSvc.getById(id);
    return {
      success: true,
      data,
      message: 'Product ok',
    } as any;
  }

  @Get('cover/:name')
  async getOriginalImage(@Param('name') name, @Res() res) {
    const options = {
      root: '../images/',
      headers: {
        'Content-Type': 'image/jpeg',
      },
    };
    if (fs.existsSync(options.root + name)) {
      res.sendFile(name, options);
    } else {
      res.sendStatus(404);
    }
  }

  @Post()
  create(@Body() newItem: CreateProductsDto): Promise<Product> {
    return this.itemsSvc.addProduct(newItem);
  }

  @Put(':id')
  updateItem(@Param('id') itemId, @Body() updateItem: CreateProductsDto): Promise<Product | any> {
    return this.itemsSvc.updateProduct(itemId, updateItem);
  }

  @Delete(':id')
  removeItem(@Param('id') itemId): Promise<Product | any> {
    return this.itemsSvc.removeProduct(itemId);
  }

}
