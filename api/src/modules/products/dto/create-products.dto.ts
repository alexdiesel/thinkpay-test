export class CreateProductsDto {
  readonly name: string;
  readonly code: string;
  readonly price: number;
}
