import { CreateProductsDto } from './dto/create-products.dto';
import { Product } from './interfaces/products.interface';
import { ProductsService } from './products.service';
export declare class ProductsController {
    private itemsSvc;
    constructor(itemsSvc: ProductsService);
    getItems(): Promise<Product[]>;
    getItem(id: any): Promise<any>;
    getOriginalImage(name: any, res: any): Promise<void>;
    create(newItem: CreateProductsDto): Promise<Product>;
    updateItem(itemId: any, updateItem: CreateProductsDto): Promise<Product | any>;
    removeItem(itemId: any): Promise<Product | any>;
}
