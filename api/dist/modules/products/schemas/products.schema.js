"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsSchema = void 0;
const mongoose = require("mongoose");
exports.ProductsSchema = new mongoose.Schema({
    name: String,
    code: String,
    price: Number,
    created_at: {
        type: Date,
        default: Date.now,
    },
    updated_at: {
        type: Date,
        default: Date.now,
    },
    cover: String,
});
//# sourceMappingURL=products.schema.js.map