"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsController = void 0;
const common_1 = require("@nestjs/common");
const fs = require("fs");
const create_products_dto_1 = require("./dto/create-products.dto");
const products_service_1 = require("./products.service");
let ProductsController = class ProductsController {
    constructor(itemsSvc) {
        this.itemsSvc = itemsSvc;
    }
    async getItems() {
        const data = await this.itemsSvc.getAll();
        return {
            success: true,
            data: data || [],
            message: 'Products retrieved successfully',
        };
    }
    async getItem(id) {
        const data = await this.itemsSvc.getById(id);
        return {
            success: true,
            data,
            message: 'Product ok',
        };
    }
    async getOriginalImage(name, res) {
        const options = {
            root: '../images/',
            headers: {
                'Content-Type': 'image/jpeg',
            },
        };
        if (fs.existsSync(options.root + name)) {
            res.sendFile(name, options);
        }
        else {
            res.sendStatus(404);
        }
    }
    create(newItem) {
        return this.itemsSvc.addProduct(newItem);
    }
    updateItem(itemId, updateItem) {
        return this.itemsSvc.updateProduct(itemId, updateItem);
    }
    removeItem(itemId) {
        return this.itemsSvc.removeProduct(itemId);
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getItems", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getItem", null);
__decorate([
    common_1.Get('cover/:name'),
    __param(0, common_1.Param('name')),
    __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "getOriginalImage", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_products_dto_1.CreateProductsDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, create_products_dto_1.CreateProductsDto]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "updateItem", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProductsController.prototype, "removeItem", null);
ProductsController = __decorate([
    common_1.Controller('products'),
    __metadata("design:paramtypes", [products_service_1.ProductsService])
], ProductsController);
exports.ProductsController = ProductsController;
//# sourceMappingURL=products.controller.js.map