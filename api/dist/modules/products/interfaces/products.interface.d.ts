export interface Product {
    _id?: string;
    name: string;
    code?: string;
    price?: number;
    cover?: string;
    created_at?: string;
    updated_at?: string;
}
