import { Model } from 'mongoose';
import { Product } from './interfaces/products.interface';
export declare class ProductsService {
    private readonly productModel;
    constructor(productModel: Model<Product | any>);
    getAll(): Promise<Product[]>;
    getById(id: string): Promise<Product>;
    addProduct(product: Product): Promise<Product>;
    updateProduct(productId: string, updateItem: Product): Promise<Product>;
    removeProduct(productId: string): Promise<Product>;
}
